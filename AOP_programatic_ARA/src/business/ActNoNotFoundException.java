package business;

public class ActNoNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2972817412499079025L;

	@Override
	public String toString() {
		return "account number is invalid";
	}

}
